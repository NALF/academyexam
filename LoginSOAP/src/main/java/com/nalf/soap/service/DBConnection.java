package com.nalf.soap.service;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.jaom.ws.trainings.User;

public class DBConnection {
	
	public static Connection getConnection() {
		final String JDBC_DRIVER ="com.mysql.jdbc.Driver";
		final String DB_URL = "jdbc:mysql://localhost:3306/javaproject"+
		"?createDatabaseIfNotExist=true&serverTimezone=UTC#";
		final String USER = "root";
		final String PASS = "1234";
		
		Connection conn = null;
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL,USER,PASS);
		}catch(Exception e) {
			System.out.println(e);
		}
		return conn;
	}
	public static List<User> getUsers(){
		List<User> users = new ArrayList<User>();
		try {
			PreparedStatement ps = DBConnection.getConnection().prepareStatement("SELECT * FROM user");
			ResultSet rs = ps.executeQuery();
			User us;
			while(rs.next()) {
				us = new User();
				us.setId(BigInteger.valueOf(rs.getInt("id")));
				us.setName(rs.getString("name"));
				us.setPassword(rs.getString("password"));
				users.add(us);
			}
		}catch(Exception e) {
			System.out.println(e+"ERROR");
			return users;
		}
		return users;

	}

}
