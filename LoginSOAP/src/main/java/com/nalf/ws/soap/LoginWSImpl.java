package com.nalf.ws.soap;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.cxf.feature.Features;

import com.jaom.ws.trainings.GetLoginRequest;
import com.jaom.ws.trainings.GetLoginResponse;
import com.jaom.ws.trainings.LoginPortType;
import com.jaom.ws.trainings.User;
import com.nalf.soap.service.DBConnection;

@Features(features = "org.apache.cxf.feature.LoggingFeature")
public class LoginWSImpl implements LoginPortType {

	Map<BigInteger, List<User>> users = new HashMap<>();
	int currentId;
	
	public LoginWSImpl() {
		init();
	}

	private void init() {
		List<User> users = DBConnection.getUsers();
		System.out.println(users.size());
		
	}

	public GetLoginResponse getLogin(GetLoginRequest request) {
		String name = request.getName();
		String passwd = request.getPassword();
		List<User> users = DBConnection.getUsers();
		
		GetLoginResponse response = new GetLoginResponse();
		
		for(int i=0; i<users.size();i++) {
			if(users.get(i).getName().equals(name) &&
					users.get(i).getPassword().equals(passwd)) {
				response.setResult(true);
				return response;
			}
		}
		response.setResult(false);
		return response;
	}


}
