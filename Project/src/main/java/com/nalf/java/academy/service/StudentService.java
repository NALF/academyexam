package com.nalf.java.academy.service;

import java.util.List;

import com.nalf.java.academy.model.Student;


public interface StudentService {
	List<Student> getAll();
	void save(Student s);
	Student getById(long id);
	String getByNameAndMonth(String name, String month);
	List<Student> getAllByMonth(String month);
	List<Student> getByIsAndPeriod(String name, String start, String end);
	List<Student> getAllByPeriod(String startDate, String endDate);
}
