package com.nalf.java.academy.controller;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nalf.java.academy.model.Student;
import com.nalf.java.academy.service.MyErrorService;
import com.nalf.java.academy.service.StudentService;

@RestController
@RequestMapping("/api/v1")
public class StudentController {

	@Autowired
	private StudentService service;
	
	
	@PostMapping("users")
	public List<Student> students(@RequestParam("is") String IS, @RequestParam("start") 
		String startDate, @RequestParam("end") String endDate) throws Exception {
		List<Student> lista =service.getByIsAndPeriod(IS, startDate, endDate);
		if(lista.size()<=0) {
			throw new MyErrorService("No data found");
		}
		return lista;		
	}
	
	@GetMapping("users/{is}/{mes}")
	public String getHoursMonth(@PathVariable String is, @PathVariable String mes){
		String hours = service.getByNameAndMonth(is, mes);
		if(hours.equals("0:0:0")) {
			throw new MyErrorService("No data found"); 
		}
		return hours;
	}
	
	@PostMapping("period")
	public List<Student> period(@RequestParam("start") String startDate, @RequestParam("end") String endDate) {
		List<Student> lista = service.getAllByPeriod(startDate, endDate);
		if(lista.size()<=0) {
			throw new MyErrorService("No data found"); 
		}
		return lista;
	}
	
	@GetMapping("period/{mes}")
	public List<Student> period2(@PathVariable String mes) {
		List<Student> lista =service.getAllByMonth(mes);
		if(lista.size()<=0) {
			throw new MyErrorService("No data found"); 
		}
		return lista;
	}
	@RequestMapping(value="init", method = RequestMethod.GET)
	public void init() {
		//String nombreArchivo = "Inventario.xlsx";
				String rutaArchivo = "Registros Momentum.xls";

				try (FileInputStream file = new FileInputStream(new File(rutaArchivo))) {
					HSSFWorkbook workbook = new HSSFWorkbook(file);
					Sheet sheet = workbook.getSheetAt(0);
					Iterator<Row> rowIterator = sheet.iterator();

					Row row;
					rowIterator.next();
					Row rowNext=rowIterator.next();
					ArrayList<Student> registros = new ArrayList<>();
					
					int i = 1;
				
					while (rowIterator.hasNext()) {
						row = rowNext;
						rowNext =rowIterator.next();
						
						
						/*IS*/
						String nombre = row.getCell(2).getStringCellValue();
						String nombre2= rowNext.getCell(2).getStringCellValue();
						//System.out.println(nombre+" "+ nombre2);
						
						Student regActual= new Student();
						Student regNext = new Student();
						regActual.setName(nombre);
						regNext.setName(nombre2);
						
						/*DATE*/
						String sDate1=row.getCell(4).getStringCellValue(); 
						String sDate2=rowNext.getCell(4).getStringCellValue(); 
						String [] a = sDate1.split(" ");
						String [] b = a[0].split("-");
						String [] c = a[1].split(":");
						
						
						Date date = new Date(); date.setDate(Integer.parseInt(b[2]));date.setMonth(Integer.parseInt(b[1])-1); date.setYear(Integer.parseInt(b[0]));
						date.setHours(Integer.parseInt(c[0]));date.setMinutes(Integer.parseInt(c[1])); date.setSeconds(Integer.parseInt(c[2]));

						String [] a1 = sDate2.split(" ");
						String [] b1 = a1[0].split("-");
						String [] c1 = a1[1].split(":");
						
						Date date2 = new Date(); date2.setDate(Integer.parseInt(b1[2]));date2.setMonth(Integer.parseInt(b1[1])-1); date2.setYear(Integer.parseInt(b1[0]));
						date2.setHours(Integer.parseInt(c1[0]));date2.setMinutes(Integer.parseInt(c1[1])); date2.setSeconds(Integer.parseInt(c1[2]));

						String id = row.getCell(3).getStringCellValue(); 
						/*In - Out*/
						String aux = row.getCell(6).getStringCellValue();
						String aux2 = rowNext.getCell(6).getStringCellValue();
						
						if(nombre.equals(nombre2)) {
							if(date2.getDate()==date.getDate()) {
								if(date2.getTime()>date.getTime()) {
									regActual.setEntrada(a[1]);
									regActual.setSalida(a1[1]);
									regActual.setDia(a[0]);
									regActual.setId(id);
									regActual.setIdStudent(i);
									i++;
									registros.add(regActual);
									service.save(regActual);
								}else {
									//System.out.println("salto");
									//row=rowIterator.next();
								}
							}else {
								//System.out.println("salto");
								//row=rowIterator.next();
							}
						}else {
							//System.out.println("salto");
							//row=rowIterator.next();
						}

						if(registros.size()>=2) {

							Student reg1 = registros.get(i-3);
							Student reg2 = registros.get(i-2);
							String d1 = reg1.getDia().split("-")[2];
							String d2 = reg2.getDia().split("-")[2];
							if(d1.equals(d2)) {
								registros.get(i-2).setSalida(reg2.getSalida());
								//service.save(registros.get(i-2));
								registros.remove(i-3);
								i--;
							}
						}
						
					}
					//registros.forEach(r -> service.save(r));
					workbook.close();
				} catch (Exception e) {
					throw new MyErrorService("File not found");
				}
				
	}
	
	
}
