package com.nalf.java.academy.model;


import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Student {
	
	
	@Id
	private int id_student;
	private String name;
	private String id;
	private String entrada;
	private String salida;
	private String dia;
	
	public Student() {}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEntrada() {
		return entrada;
	}
	public void setEntrada(String entrada) {
		this.entrada = entrada;
	}
	
	public int getId_student() {
		return id_student;
	}
	public void setId_student(int id_student) {
		this.id_student = id_student;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSalida() {
		return salida;
	}
	public void setSalida(String salida) {
		this.salida = salida;
	}
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	
	
	public int getIdStudent() {
		return id_student;
	}
	public void setIdStudent(int idStudent) {
		this.id_student = idStudent;
	}
	@Override
	public String toString() {
		return "Student [id_student=" + id_student + ", name=" + name + ", id=" + id + ", entrada=" + entrada
				+ ", salida=" + salida + ", dia=" + dia + "]";
	}
	
	
	
}
