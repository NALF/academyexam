package com.nalf.java.academy.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nalf.java.academy.model.MyError;

@Controller
public class MyErrorController implements ErrorController {


	  @RequestMapping("/error")
	  @ResponseBody
	  public MyError handleError(HttpServletRequest request) {
	      Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
	      Exception exception = (Exception) request.getAttribute("javax.servlet.error.exception");
	      return new MyError(exception.getCause().getMessage(), statusCode);
	  }

	  @Override
	  public String getErrorPath() {
	      return "/error";
	  }
}
