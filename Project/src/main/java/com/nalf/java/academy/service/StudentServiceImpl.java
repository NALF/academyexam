package com.nalf.java.academy.service;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nalf.java.academy.model.Student;
import com.nalf.java.academy.repository.StudentRepository;

@Service
public class StudentServiceImpl implements StudentService{

	@Autowired
	private StudentRepository repo;
	
	@Override
	public List<Student> getAll() {
		return repo.findAll();
	}

	@Override
	public Student getById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Student s) {
		repo.save(s);
	}

	@Override
	public String getByNameAndMonth(String name, String month) {
		// TODO Auto-generated method stub
		month="-"+month+"-";
		List<Student> list = repo.getByNameAndMonth(name, month);
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		Date date1 = new Date();
		Date date2 = new Date();
		Long millis1, millis2;
		Long suma= 0L;
		for(int i=0; i<list.size();i++) {
			try {
				date1 = sdf.parse(list.get(i).getEntrada());
				date2 = sdf.parse(list.get(i).getSalida());
				millis1 = date1.getTime();
				millis2 = date2.getTime();
				suma+=(millis2-millis1);
			} catch (ParseException e) {
				System.err.println(e.getMessage());
			}
			
		}
		Long hrs = suma/3600000;
		Long sobra = suma%3600000;
		Long mins = sobra/60000;
		sobra = mins%60000;
		Long seg = sobra/1000;
		return hrs+":"+mins+":"+seg;
	}

	@Override
	public List<Student> getAllByMonth(String month) {
		// TODO Auto-generated method stub
		month="-"+month+"-";
		return repo.getAllByMonth(month);
	}

	@Override
	public List<Student> getByIsAndPeriod(String name, String start, String end) {
		return getListByPeriod(start,end,repo.getByIsAndPeriod(name));
	}

	@Override
	public List<Student> getAllByPeriod(String start, String end) {
		return getListByPeriod(start,end,repo.findAll());
	}
	
	public List<Student> getListByPeriod(String start, String end, List<Student> list) {
		Date actualDate = new Date();
		Date startDate = new Date();
		Date endDate = new Date();
		
		startDate.setYear(Integer.parseInt(start.substring(0,4)));
		startDate.setMonth(Integer.parseInt(start.substring(5,7)));
		startDate.setDate(Integer.parseInt(start.substring(8,10)));
		
		endDate.setYear(Integer.parseInt(end.substring(0,4)));
		endDate.setMonth(Integer.parseInt(end.substring(5,7)));
		endDate.setDate(Integer.parseInt(end.substring(8,10)));
		
		String actual;
		
		for(int i=0; i<list.size(); i++) {
			actual=list.get(i).getDia();
			actualDate.setYear(Integer.parseInt(actual.substring(0,4)));
			actualDate.setMonth(Integer.parseInt(actual.substring(5,7)));
			actualDate.setDate(Integer.parseInt(actual.substring(8,10)));
			if(actualDate.before(startDate) || actualDate.after(endDate)) {
				list.remove(i);
				i--;
			}
		}
		return list;
	}
	
	

	
}
