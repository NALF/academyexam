package com.nalf.java.academy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.nalf.java.academy.model.Student;

public interface StudentRepository extends JpaRepository <Student, Integer>{
	 @Query(
	            "SELECT s FROM Student s WHERE s.dia LIKE %?2% AND s.name = ?1"
	    )
	List<Student> getByNameAndMonth(String name, String month);
	 
	 @Query(
	            "SELECT s FROM Student s WHERE s.dia LIKE %?1%"
	    )
	List<Student> getAllByMonth(String month);

	 @Query(
	            "SELECT s FROM Student s WHERE s.name LIKE ?1"
	    )
	List<Student> getByIsAndPeriod(String name);
}
