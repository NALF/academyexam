package com.nalf.java.academy.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nalf.java.academy.model.Student;
import com.nalf.java.academy.service.MyErrorService;
import com.nalf.java.academy.service.StudentService;

@Controller
public class IndexController {

	@Autowired
	private StudentService service;
	
    @RequestMapping("/")
    public String home(Map<String, Object> model) {
        model.put("message", "HowToDoInJava Reader !!");
        return "index";
    }
     
    @RequestMapping("/users")
    public String next(Map<String, Object> model) {
        return "UserHrsMonth";
    }
    
    @PostMapping("/users/result")
 	public String method(@RequestParam String is, @RequestParam String mes, Model m) {
    	String hours = service.getByNameAndMonth(is, mes);
		
		m.addAttribute("result",hours);
		m.addAttribute("name",is);
		m.addAttribute("mes",mes);
    	return "UserResult";
    }
    /*****************************************************/
    @RequestMapping("/users/period")
    public String next3(Map<String, Object> model) {
        return "UserHrsPeriod";
    }
    @PostMapping("/users/period/result")
 	public String method2(@RequestParam String is, String inicio, String fin, Model m) {
    	List<Student> lista =service.getByIsAndPeriod(is, inicio, fin);
		m.addAttribute("result",lista);
		m.addAttribute("name",is);
		m.addAttribute("inicio",inicio);
		m.addAttribute("fin",fin);
    	return "UserHrsPeriodResult";
    }
    /*****************************************************/
    @RequestMapping("/users/assist")
    public String next4(Map<String, Object> model) {
        return "AssistMonth";
    }
    @PostMapping("/users/assist/result")
 	public String method3(@RequestParam String mes, Model m) {
        List<Student> lista =service.getAllByMonth(mes);
        
		m.addAttribute("lista",lista);
		m.addAttribute("mes",mes);
    	return "AssistMonthResult";
    }
    /*****************************************************/
    @RequestMapping("/period/assist")
    public String next5(Map<String, Object> model) {
        return "AssistPeriod";
    }
    @PostMapping("/period/assist/result")
 	public String method5(@RequestParam String inicio, String fin, Model m) {
		List<Student> lista = service.getAllByPeriod(inicio, fin);
        
		m.addAttribute("lista",lista);
		m.addAttribute("inicio",inicio);
		m.addAttribute("fin",inicio);
    	return "AssistPeriodResult";
    }
    
    
}
