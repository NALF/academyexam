<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
         <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Horas por periodo</title>
</head>
<body>
<h4>Tiempo de trabajo en el periodo ${inicio} a ${fin}</h4>
<h5>IS: ${name}</h5>
<table border="1">
<tr>
	<th>ID</th>
	<th>Dia</th>
	<th>Entrada</th>
	<th>Salida</th>
</tr>
<tr>

<c:forEach items="${result}" var="list">
 <td> <h4>${list.id}</h4></td>
  <td>${list.dia}</td>
  <td>${list.entrada}</td>    
 <td>${list.salida}</td> 

 </tr>             
</c:forEach>

</table>
<a href = "http://localhost:8080/exam/"> Regresar al Menu</a>
</body>
</html>