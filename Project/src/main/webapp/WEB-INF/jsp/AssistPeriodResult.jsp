<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
             <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Asistencia</title>
</head>
<body>
<h2>Asistencia del periodo ${inicio} a ${fin}</h2>
<table border="1">
<tr>
	<th>IS</th>
	<th>Dia</th>
	<th>Entrada</th>
	<th>Salida</th>
</tr>
<tr>

<c:forEach items="${lista}" var="list">
 <td>${list.name}</td>
  <td>${list.dia}</td> 
  <td>${list.entrada}</td>
   <td>${list.salida}</td>
  
 </tr>             
</c:forEach>

</table>
<a href = "http://localhost:8080/exam/"> Regresar al Menu</a>
</body>
</html>