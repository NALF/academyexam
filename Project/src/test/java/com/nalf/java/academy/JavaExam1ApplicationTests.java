package com.nalf.java.academy;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.nalf.java.academy.model.Student;
import com.nalf.java.academy.repository.StudentRepository;
import com.nalf.java.academy.service.StudentService;


@SpringBootTest
class JavaExam1ApplicationTests {

	@Autowired
	private StudentService studentService;
	@Autowired
	private StudentRepository repo;
	
	@Test
	public void getStudentsTestUnit() {
		List<Student> students= repo.findAll();
		int actual = students.size();
		int notExpected = 0;
		assertNotSame(notExpected, actual);
	}
	@Test
	public void getFirstStudentTestUnit() {
		List<Student> students= repo.findAll();
		String actual = students.get(0).getName();
		String expected = "CXMG";
		assertEquals(expected, actual);
	}
	@Test
	public void getLastStudentTestUnit() {
		List<Student> students= repo.findAll();
		String actual = students.get(students.size()-1).getName();
		String expected = "JFDG1";
		assertEquals(expected, actual);
	}
	@Test
	public void getStudentsTest() {
		List<Student> students= studentService.getAll();
		int actual = students.size();
		int notExpected = 0;
		assertNotSame(notExpected, actual);
	}
	
}
